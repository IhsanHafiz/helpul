<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
    
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Laporan
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>No HP</th>
                                    <th>Kode Pos</th> 
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Kota</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($laporans as $laporan)
                                <tr>
                                    <td>{{$laporan->nama}}</td>
                                    <td>{{$laporan->no_hp}}</td>
                                    <td>{{$laporan->kode_pos}}</td>
                                    <td>{{$laporan->email}}</td>
                                    <td>{{$laporan->alamat}}</td>
                                    <td>{{$laporan->kota}}</td>
                                </tr>
                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Barang</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                             @if(Cart::count() > 0 )
                             <ul class="products-cart">
                                @foreach (Cart::content() as $item)
                                   <td class="product-image" style="display: flex">
                                      <a class="link-to-product" href="{{route('product.details', ['slug'=>$item->model->slug])}}">{{$item->model->name}}</a>
                                   </td>
                                   <td class="price-field produtc-price" ><p style="color: black">{{$item->qty}} barang</p></td>
                                @endforeach					
                             </ul>
                             @else
                                <p>Keranjangnya kosong</p>
                                @endif 
                            </tbody>
                        </table>
                        {{-- {{$laporans->links()}} --}}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</body>

</body>
</html>