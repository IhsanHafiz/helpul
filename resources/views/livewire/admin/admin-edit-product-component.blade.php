<div>
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Edit Barang 
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('admin.products')}}" class="btn btn-warning pull-right">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert"{{Session::get('message')}}></div>
                        @endif
                        <form class="form-horizontal" enctype="multipart/form-data" wire:submit.prevent='updateProduct'>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Barang</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Nama Barang" class="form-control input-md" wire:model='name' wire:keyup='generateSlug' />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Slug Barang</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Slug Barang" class="form-control input-md" wire:model='slug' />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Deskripsi Singkat</label>
                                <div class="col-md-4">
                                   <textarea class="form-control" placeholder="Deskripsi Singkat"  wire:model='short_description' ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Deskripsi</label>
                                <div class="col-md-4">
                                   <textarea class="form-control" placeholder="Deskripsi"  wire:model='description' ></textarea>                                    </div>
                                </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Harga</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Harga" class="form-control input-md"  wire:model='regular_price'  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">SKU</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="SKU" class="form-control input-md"  wire:model='SKU'  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Stock</label>
                                <div class="col-md-4">
                                  <select class="form-control"  wire:model='stock_status'  >
                                      <option value="instock">Instock</option>
                                      <option value="outofstock">Out of Stock</option>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Quantity</label>
                                <div class="col-md-4">
                                  <input type="text" placeholder="Quantity" class="form-control input-md"  wire:model='quantity'  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Gambar</label>
                                <div class="col-md-4">
                                  <input type="file" class="input-file"  wire:model='image'  />
                                  @if($newimage)
                                        <img src="{{$newimage->temporaryUrl()}}" width="120" />
                                  @else 
                                    <img src="{{asset('assets/images/products')}}/{{$image}}" width="120"/>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Kategori</label>
                                <div class="col-md-4">
                                    <select class="form-control"  wire:model='category_id' >
                                        <option value="">Pilih Kategori</option>
                                         @foreach ($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>      
                                         @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success">Perbarui</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
