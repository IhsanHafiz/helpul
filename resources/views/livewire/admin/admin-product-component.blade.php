<div>
    <style>
        nav svg{
            height: 20px;

        }
        nav .hidden{
            display: block !important;
        }

    </style>
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Semua Barang
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('admin.addproduct')}}" class="btn btn-success pull-right">Tambahkan</a>
                            </div>
                        </div>   
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Gambar</th>
                                    <th>Nama Barang</th>
                                    <th>Stock</th>
                                    <th>Harga</th>
                                    <th>Kategori</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td><img src="{{asset('assets/images/products')}}/{{$product->image}}" width="60" /></td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->stock_status}}</td>
                                        <td>Rp {{number_format($product->regular_price, 0,',', '.')}}</td>
                                        <td>{{$product->category->name}}</td>
                                        <td>
                                            <a href="{{route('admin.editproduct', ['product_slug'=>$product->slug])}}" style="color: black"><i class="fa fa-edit fa-2x" style="padding: 0 5px; "></i>Edit</a>
                                            <a href="#" wire:click.prevent='deleteProduct({{$product->id}})' style="color: black"><i class="fa fa-times fa-2x" style="padding:0 5px; color:red"></i>Hapus</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$products->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
