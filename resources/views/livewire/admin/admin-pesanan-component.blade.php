<div>
    <style>
        nav svg{
            height: 20px;
        }
         nav .hidden{
             display: block !important;
         }

    </style>
    <div class="container" style="padding:30px 0;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Semua Kategori
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('admin.addcategory')}}" class="btn btn-success pull-right">Kategori Baru</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>No HP</th>
                                <th>Kode Pos</th> 
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pesanans as $pesanan)
                            <tr>
                                <td>{{$pesanan->id}}</td>
                                <td>{{$pesanan->nama}}</td>
                                <td>{{$pesanan->no_hp}}</td>
                                <td>{{$pesanan->kode_pos}}</td>
                                <td>{{$pesanan->email}}</td>
                                <td>{{$pesanan->alamat}}</td>
                                <td>{{$pesanan->kota}}</td>
                            </tr>
                                
                            @endforeach
                        </tbody>
                    </table>
                    {{$pesanans->links()}}
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
