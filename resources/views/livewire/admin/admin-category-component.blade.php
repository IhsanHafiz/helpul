<div>
    <style>
        nav svg{
            height: 20px;
        }
         nav .hidden{
             display: block !important;
         }

    </style>
    <div class="container" style="padding:30px 0;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Semua Kategori
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('admin.addcategory')}}" class="btn btn-success pull-right">Kategori Baru</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama Kategori</th>
                                <th>Slug</th>
                                <th>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->Slug}}</td>
                                <td style="padding: 0 5px">
                                    <a href="{{route('admin.editcategory', ['category_slug'=>$category->slug])}}" style="color: black"><i class="fa fa-edit fa-2x" style="padding: 0 5px; "></i>Edit</a>
                                    <a href="#" wire:click.prevent='deleteCategory({{$category->id}})' style="color: black"><i class="fa fa-times fa-2x" style="padding:0 5px; color:red"></i>Hapus</a>
                                </td>
                            </tr>
                                
                            @endforeach
                        </tbody>
                    </table>
                    {{$categories->links()}}
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
