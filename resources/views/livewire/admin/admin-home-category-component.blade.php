<div>
    <div class="container" style="padding: 30px 0px">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Atur Kategori di Homepage
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                        @endif
                        <form action="" class="form-horizontal" wire:submit.prevent='updateHomeCategory' > 
                            <div class="form-group">
                                <label class="col-md-4 control-label">Pilih Kategori</label>
                                <div class="col-md-4" wire:ignore>
                                    <select name="categories[]" multiple='multiple' class="sel_categories form-control" wire:model="selected_categories">
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Jumlah barang yang ditampilkan</label>
                                <div class="col-md-4">
                                   <input type="text" class="form-control input-md" wire:model='numberofproducts' />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script  type="text/javascript">
        $(document).ready(function(){
            $('.sel_categories').select2();
            $('.sel_categories').on('change', function(e){
                var data = $('.sel_categories').select2('val');
                @this.set('selected_categories', data);
            });
        });
    </script>
@endpush