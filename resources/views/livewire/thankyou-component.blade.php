<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/home" class="link">beranda</a></li>
                <li class="item-link"><span>Thank You</span></li>
            </ul>
        </div>
    </div>
    
    <div class="container pb-60">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Terima Kasih Atas Pesanan Anda</h2>
                <a href="/shop" class="btn btn-submit btn-submitx">Lanjut Belanja</a>
            </div>
        </div>
    </div><!--end container-->

</main>