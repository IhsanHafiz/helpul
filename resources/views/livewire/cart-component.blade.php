<main id="main" class="main-site">

   <div class="container">

      <div class="wrap-breadcrumb">
         <ul>
            <li class="item-link"><a href="/" class="link">BERANDA</a></li>
            <li class="item-link"><span>KERANJANG</span></li>
         </ul>
      </div>
      <div class=" main-content-area">

         <div class="wrap-iten-in-cart">
            @if(Session::has('success_message'))
               <div class="alert alert-success">
                  <strong>Berhasil!</strong> {{Session::get('success_message')}}
               </div>
            @endif
            @if(Cart::count() > 0 )
            <h3 class="box-title">Barang di Keranjang</h3>
            <ul class="products-cart">
               @foreach (Cart::content() as $item)
               <li class="pr-cart-item">
                  <div class="product-image">
                     <figure><img src="{{ asset('assets/images/products') }}/{{$item->model->image}}" alt="{{$item->model->name}}"></figure>
                  </div>
                  <div class="product-name">
                     <a class="link-to-product" href="{{route('product.details', ['slug'=>$item->model->slug])}}">{{$item->model->name}}</a>
                  </div>
                  <div class="price-field produtc-price"><p class="price">Rp {{number_format($item->model->regular_price, 0,',', '.')}}</p></div>
                  <div class="quantity">
                     <div class="quantity-input">
                        <input type="text" name="product-quatity" value="{{$item->qty}}" data-max="120" pattern="[0-9]*" >									
                        <a class="btn btn-increase" href="#" wire:click.prevent="increaseQuantity('{{$item->rowId}}')"></a>
                        <a class="btn btn-reduce" href="#" wire:click.prevent="decreaseQuantity('{{$item->rowId}}')"></a>
                     </div>
                  </div>
                  <div class="price-field sub-total"><p class="price">Rp {{number_format($item->model->regular_price, 0,',', '.')}}</p></div>
                  <div class="delete">
                     <a href="#" wire:click.prevent="destroy('{{$item->rowId}}')" class="btn btn-delete" title="">
                        <span>Delete from your cart</span>
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                     </a>
                  </div>
               </li>		
               @endforeach					
            </ul>
            @else
               <p>Keranjangnya kosong</p>
               @endif 
         </div>

         <div class="summary">
            <div class="order-summary">
               <h4 class="title-box">RANGKUMAN PEMESANAN</h4>
               <p class="summary-info"><span class="title">Subtotal</span><b class="index">Rp {{Cart::subtotal()}}</b></p>
               <p class="summary-info"><span class="title">Pajak</span><b class="index">Rp {{Cart::tax()}}</b></p>
               <p class="summary-info"><span class="title">Ongkir</span><b class="index">Gratis Ongkir</b></p>
               <p class="summary-info total-info "><span class="title">Total</span><b class="index">Rp {{Cart::total()}}</b></p>
            </div>
            <div class="checkout-info">
               <a class="btn btn-checkout" href="/checkout">Checkout <i class="fa fa-shopping-cart"></i></a>
               <a class="btn btn-checkout" href="#" wire:click.prevent='destroyAll()'>Hapus Semua Barang  <i class="fa fa-trash-o"></i></a>
               <a class="btn btn-checkout" href="/shop">Lanjut Belanja <i class="fa fa-shopping-basket"></i></a>
            </div>
         </div>

      </div><!--end main content area-->
   </div><!--end container-->

</main>