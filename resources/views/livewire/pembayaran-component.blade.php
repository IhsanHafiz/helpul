<div>
   <div class="container" style="width: 600;">
        <div class="row" style="display: block; justify-content: center">
            <div class="top" style="margin: 40px 295px 0px 295px;text-align: center;border: 2px solid #444444; border-radius: 10px ">
               <h3><strong>Selesai Kan Pembayaran</strong></h3>
               <strong><h4 id="countdown" class="timer" style="color: red; font-weight: 700"></h4></strong>
                </div>
            </div>
            <div class="middle" style="margin: 10px 295px; display:block">
                {{-- <h4><strong>Alfamart</strong></h4> --}}
                <img src="{{asset('assets/images/alfamart.png')}}" alt="alfamart" style="width:64px; height:30px; text-align:right;" />
                <h3>Kode Pembayaran</h3>
                <?php

                    function generateKey(){
                        $keyLength = 8;
                        $str = "12345678901234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        $randStr = substr(str_shuffle($str), 0, $keyLength);
                        return $randStr;
                    }

                    echo "<h4 style='color:red;'>" . "AA" . generateKey() . "</h4>";

                    ?>

                <h3>Total Pembayaran</h3>
                <h4 class="summary-info grand-total" style="color: red"><span class="grand-total-price">Rp {{Cart::total()}}</span></h4>
                <a href="/shop" class="btn btn-submit btn-submitx" style="width: 100%; border-radius:10px" >Lanjut Belanja</a>
                 </div>
             </div>
        </div>
   </div>
</div>

<script type="text/javascript">
   var upgradeTime = 172801;
var seconds = upgradeTime;
function timer() {
  var days        = Math.floor(seconds/24/60/60);
  var hoursLeft   = Math.floor((seconds) - (days*86400));
  var hours       = Math.floor(hoursLeft/3600);
  var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
  var minutes     = Math.floor(minutesLeft/60);
  var remainingSeconds = seconds % 60;
  function pad(n) {
    return (n < 10 ? "0" + n : n);
  }
  document.getElementById('countdown').innerHTML = pad(days) + ":" + pad(hours) + ":" + pad(minutes) + ":" + pad(remainingSeconds);
  if (seconds == 0) {
    clearInterval(countdownTimer);
    document.getElementById('countdown').innerHTML = "Completed";
  } else {
    seconds--;
  }
}
var countdownTimer = setInterval('timer()', 1000);
</script>
