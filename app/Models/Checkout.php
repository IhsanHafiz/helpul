<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;
    protected $table="checkouts";

    protected $fillable = 
    ['nama', 'email', 'no_hp','alamat', 'kode_pos', 'kota'];
}
