<?php

namespace App\Http\Livewire;

use App\Models\Checkout;
use Livewire\Component;
use Livewire\WithPagination;

class PDFComponent extends Component

{
    use WithPagination;
    public function render()
    {
        return view('livewire.p-d-f-component');
    }
}
