<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class AdminAddCategory extends Component
{
    public function render()
    {
        return view('livewire.admin.admin-add-category');
    }
}
