<?php

namespace App\Http\Livewire\Admin;

use App\Models\Checkout;
use Livewire\Component;
use Livewire\WithPagination;

class AdminPesananComponent extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.admin.admin-pesanan-component', ['pesanans'=> Checkout::latest()->paginate(10)])->layout('layouts.base');
    }
}
