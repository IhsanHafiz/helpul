<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class UsernDashboardComponent extends Component
{
    public function render()
    {
        return view('livewire.user.usern-dashboard-component');
    }
}
