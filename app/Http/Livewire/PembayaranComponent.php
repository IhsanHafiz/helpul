<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PembayaranComponent extends Component
{
    public function render()
    {
        return view('livewire.pembayaran-component')->layout('layouts.base');
    }
}
