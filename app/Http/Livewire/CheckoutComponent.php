<?php

namespace App\Http\Livewire;

use App\Models\Checkout;
use App\Models\Product;
use Livewire\Component;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Auth;

class CheckoutComponent extends Component
{
    public $nama;
    public $email;
    public $no_hp;
    public $alamat;
    public $kode_pos;
    public $kota;

    // public function destroyAll()
    // {
    //     Cart::destroy();
    // }


    public function render()
    {
        return view('livewire.checkout-component')->layout('layouts.base');
    }
    
    public function mount()
    {
        if(!Auth::user()){
            return redirect()->route('/');
        }
    }

    public function destroyAll()
    {
        Cart::destroy();
        session()->flash('success_message', 'Semua barang telah dihapus');
    }

    public function store()
    {
        Checkout::create([
            'nama' => $this->nama,
            'email' =>$this->email,
            'no_hp' => $this->no_hp,
            'alamat' =>$this->alamat,
            'kode_pos' =>$this->kode_pos,
            'kota' =>$this->kota
        ]);
    }
}
