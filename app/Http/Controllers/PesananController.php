<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pesanan;

class PesananController extends Controller
{
    function addData(Request $req)
    {
        $pesanan = new Pesanan;
        $pesanan->nama = $req->nama;
        $pesanan->no_hp = $req->no_hp;
        $pesanan->kode_pos = $req->kode_pos;
        $pesanan->email = $req->email;
        $pesanan->alamat = $req->alamat;
        $pesanan->kota = $req->kota;
        $pesanan->save();
        return redirect('/');
    }
}
