<?php

namespace App\Http\Controllers;

use App\Models\Checkout;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function getPDF()
    {
        $this->laporans = Checkout::latest();

        $laporans = Checkout::all();
        return view('pdf', compact('laporans'));
    }

    // public function pdf()
    // {
    //     $laporans = Checkout::all();
    //     $pdf = PDF::loadView('pdf',compact('laporans'));
    //     return $pdf->download('laporan.pdf');
    // }
}
